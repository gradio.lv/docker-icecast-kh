#!/bin/bash

# start from zero IMPORTANT remove all other docker images
# docker stop $(docker ps -aq); docker rm $(docker ps -aq); docker rmi $(docker images -q);
# docker system prune

docker kill rolla-icecast-kh
docker rm -f rolla-icecast-kh
docker rmi -f rolla/icecast-kh
time docker-compose -f docker-compose.yml -f docker-compose-build.yml build

# log into docker container
# docker exec -it rolla-icecast-kh bash
